public class User
{
	private String log;
	private String mdp;
	private Boolean Auth;

	public User()
	{
		log="anonymous";
		mdp="anonymous";
		Auth=false;
	}
	public User(String lg, String mp)
	{
		log=lg;
		mdp=mp;
		Auth=false;
	}
	public boolean authentificate()
	{
		return Auth;
	}
	public void setAuthentificate(boolean ath)
	{
		Auth=ath;
	}
	public String loggin()
	{
		return log;
	}
	public String password()
	{
		return mdp;
	}
	public void setLoggin(String lg)
	{
		log=lg;
	}
	public void setPassword(String mp)
	{
		mdp=mp;
	}
}