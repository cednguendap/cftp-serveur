import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.*;


public class ManagerFileSystem
{
	Path courrant;
	ManagerFileSystem()
	{
		courrant=path("./systeme_fichier/");//Doit inserer le repertoire courant
	}
	private Path path(String p) 
	{
		return FileSystems.getDefault().getPath(p).normalize();
	}
	private boolean exist(String curr)
	{
		return Files.exists(path(curr));
	}
	public void setCourrant(String curr)
	{
		courrant=path(curr);
	}
	public String getCourrant()
	{
		return courrant.toString();
	}
	public String pwd()
	{
		return courrant.toAbsolutePath().normalize().toString();
	}
	public boolean egale(String che) throws IOException
	{
		if(!exist(che)) throw new IOException("Fichier innexistant");
		return Files.isSameFile(courrant,path(che)) || courrant.compareTo(path(che))>0?true:false;
	}
	public void cd(String curr) throws IOException
	{
		if(exist(pwd()+FileSystems.getDefault().getSeparator()+curr))
		{
			setCourrant(pwd()+FileSystems.getDefault().getSeparator()+curr);
		}
		else if(!exist(curr)) throw new IOException("Repertoire innexistant");
		else setCourrant(curr);	
	}
	public void mkdir(String nom) throws IOException
	{
		if(exist(nom)) throw new IOException("Ce repertoire existe déjà");
		Files.createDirectory(path(pwd()+FileSystems.getDefault().getSeparator()+nom));
	}
	public String CheminAbsolueVers(String nom)
	{
		return path(pwd()+FileSystems.getDefault().getSeparator()+nom).toString();
	}
	public void rmdir(String nom) throws IOException 
	{
		if(!exist(nom)) throw new IOException("Repertoire innexistant");
		else if(!exist(nom)) throw new IOException("Fichier innexistant");//Detecter s'il est vide
		
	}
	public void createFile(String nom) throws IOException
	{
		if(exist(nom)) throw new IOException("Fichier existant");
		Files.createFile(courrant);
	}
	public String ls(String emplacement) throws IOException
	{
		String res="";
		String emp=emplacement.equals(" ")?"./":emplacement;
		try (DirectoryStream<Path> stream=Files.newDirectoryStream(path(pwd()+FileSystems.getDefault().getSeparator()+emp)))
		{
			for(Path entre: stream)
			{
				res=res+" \n"+entre.getName(entre.getNameCount()-1).toString();
			}
			System.out.println(res);
		} 
		catch(IOException | DirectoryIteratorException e)
		{
			throw new IOException("emplacement invalid");
		}
		finally
		{
			return res+"\n";	
		}
	}
	public String ls_l(String emplacement) throws IOException
	{
		String res="";
		String emp=emplacement.equals(" ")?"./":emplacement;
		try (DirectoryStream<Path> stream=Files.newDirectoryStream(path(pwd()+FileSystems.getDefault().getSeparator()+emp)))
		{
			int i=0;
			for(Path entre: stream)
			{

				/*PosixFileAttributes posixAttrib=Files.getFileAttributeView(entre,PosixFileAttributeView.class).readAttributes();//Permission du fichier (UNIX)
				//BasicFileAttributes posixAttrib=Files.getFileAttributeView(entre,BasicFileAttributeView.class).readAttributes();
				res+=posixAttrib.isDirectory()==true?'d':"-";
				res+=" "+PosixFilePermissions.toString(posixAttrib.permissions());
				res+=" "+posixAttrib.owner().getName()+" "+posixAttrib.group().getName();

				String[] tabj=new String[2];
				tabj=posixAttrib.creationTime().toString().split("T");

				String[] tabh=new String[2];
				tabh=tabj[1].split(".");

				res+=" "+posixAttrib.size()+" "+tabj[0]+" "+tabh[0]+" "+entre.getName(entre.getNameCount()-1).toString();
				res+="\r";
				i++;*/

				PosixFileAttributes posixAttrib=Files.getFileAttributeView(entre,PosixFileAttributeView.class).readAttributes();//Permission du fichier (UNIX)
				//BasicFileAttributes posixAttrib=Files.getFileAttributeView(entre,BasicFileAttributeView.class).readAttributes();
				res+=posixAttrib.isDirectory()==true?'d':"-";
				res+=" "+PosixFilePermissions.toString(posixAttrib.permissions());
				res+=" "+posixAttrib.owner().getName()+" "+posixAttrib.group().getName();
				res+=" "+posixAttrib.size()+" "+posixAttrib.creationTime()+" "+entre.getName(entre.getNameCount()-1).toString();
				res+=" \\r\\n ";
				i++;
			}
			res="total "+i+" \\r\\n "+res;
			System.out.println(res);

		} 
		catch(IOException | DirectoryIteratorException e)
		{
			throw new IOException("emplacement invalid");
		}
		finally
		{
			return res;	
		}
	}
	public void get(String elmt,OutputStream out) throws IOException
	{
		Files.copy(path(elmt),out);
	}
	public void put(String elmt,InputStream in) throws IOException
	{
		System.out.println(in.toString());
		Files.copy(in,path(elmt));
		/*FileOutputStream file=new FileOutputStream(path(elmt).toString());
		in.transferTo(file);
		file.close();*/
	}
	public void getImage()
	{

	}
}
