
import java.net.*;
import java.io.*;
import java.lang.NullPointerException;

public class serveur_hybrid implements Runnable
{
	//Attributs
	private Socket sock;
	private ServerSocket sockServ;
	private PrintWriter out;
	private BufferedReader in;
	private commande Cmd;
	private boolean running;
	private User user;
	private boolean mode;
	private int[] portDonne;
	private char TransfertMode;

	//Methodes
	public serveur_hybrid(Socket s,ServerSocket s1)
	{
		setSocket(s);
		sockServ=s1;
		try
		{
			out =new PrintWriter(sock.getOutputStream(),true);
			in=new BufferedReader(new InputStreamReader(sock.getInputStream()));
			running=true;
			user=new User();
			Cmd=new commande(this,user);
			mode=false; //Actif true, passif false
			portDonne=new int[6];
			TransfertMode='A';//(A)Ascil,(I)Binaire,U(Utf8)
		}
		catch(IOException e)
		{
			System.out.println("Erreur de demarrage du serveur hybrid");
			System.exit(0);
		}
		
	}
	public void run() 
	{
		try
		{
			write("220 Bienvenu sur Cftp!");
			
			String com=new String();
			String[] cmdL;
			while(running)
			{
				com=read();
				
				System.out.println("Commande: "+com);
				if(!com.equals(" "))
				{
					cmdL=com.split(" ");
					String[] result=Cmd.exec(cmdL);
					try
					{
						if(!result[0].equals(" ")) write(result[0]);
						if(!result[1].equals(" ")) write(result[1]);
						if(!result[2].equals(" ")) write(result[2]);
					}
					catch(NullPointerException e)
					{
						e.printStackTrace();
					}
				}				
			}		
			sock.close();
		}
		catch(IOException e)
		{
			System.out.println("Erreur interne");
			e.printStackTrace();
		}
		
	}
	public void setMode(boolean m)
	{
		mode=m;
	}
	public boolean mode()
	{
		return mode;
	}
	public void setRunning(boolean run)
	{
		running=run;
	}
	public  int[] portDonne()
	{
		return portDonne;
	}
	public void setPortDonne(String p)
	{
		String[] interm=p.split(",");
		for(int i=0;i<interm.length;i++)
		{
			portDonne[i]=Integer.valueOf(interm[i]).intValue();
		}
	}
	public String transfertMode()
	{
		if(TransfertMode=='A')
			return "Ascil";
		else if(TransfertMode=='I')
			return "Binaire";
		else
			return "Utf8";
	}
	public void setTransfertMode(char Mod)
	{
		if(Mod=='A' || Mod=='I' || Mod=='U') TransfertMode=Mod;
	}
	public boolean existPortDonne()
	{
		return portDonne.length==0;
	}
	protected void setSocket(Socket s)
	{
		sock=s;
	}
	public Socket getSocketDonne() throws IOException
	{
		int portP=portDonne[4]*256+portDonne[5];
		String host=portDonne[0]+"."+portDonne[1]+"."+portDonne[2]+"."+portDonne[3];
		return new Socket(host,portP);
	}
	public OutputStream OutputStreamDonne(Socket soc) throws IOException
	{
		return soc.getOutputStream();
	} 
	public InputStream InputStreamDonne(Socket soc) throws IOException
	{
		return soc.getInputStream();
	}
	/*public FileInputStream FileInputStreamDonne()
	{
		FileInputStream Finput=new FileInputStream();

		try
		{
			FinputInputStreamDonne(getSocketDonne()).readAllBytes();
		}	
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	public FileInputStream FileOutputStreamDonne()
	{
		try
		{
			(getSocketDonne());
		}	
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}*/
 	public void closeSocketDonne(Socket soc) throws IOException
	{
		soc.close();
	}
	public void write(String msg)
	{
		out.println(msg);
		out.flush();
	}
	protected void write(BufferedWriter ou, String msg)
	{
		try
		{
			ou.write(msg);
			ou.flush();
			/*
			 String[] chaine=msg.split(";");
			for(int i=0;i<chaine.length;i++)
			{
				ou.write(chaine[i]);
				ou.newLine();
			}
			 */
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}
	protected String read(BufferedReader bin)
	{
		try
		{
			return bin.readLine();
		}
		catch(IOException e)
		{
			System.out.println("Erreur interne");
			e.printStackTrace();
			return " ";
		}
	}
	protected String read()
	{
		try
		{
			return in.readLine();
		}
		catch(IOException e)
		{
			System.out.println("Erreur interne");
			e.printStackTrace();
			String el=new String();
			el=" ";
			return el;
		}
	}



	public void connexionDonneEnvoi(String donne)
	{
		try
		{
			Socket sockData=getSocketDonne();
			//DataInputStream inp=new DataInputStream(sock.getInputStream());
			//DataOutputStream outp=new DataOutputStream(sock.getOutputStream());
			//outp.writeBytes(donne);
			BufferedWriter ou = new BufferedWriter(new PrintWriter(sock.getOutputStream()));
			//BufferedReader bin=new BufferedReader(new InputStreamReader(sock.getInputStream()));
			write(ou,donne);
			sockData.close();
		}
		catch(IOException e)
		{
			System.out.println("Erreur du port de données");
			System.exit(0);
		}
	}
}

