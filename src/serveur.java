
import java.net.*;
import java.io.*;

class serveur
{
	//Attributs
	private int port;
	private String name;
	private ServerSocket sock;
	//Methodes
	public serveur(int p)
	{ 
		setport(p);
		name="Cftp";
		try
		{
			sock=new ServerSocket(p);
			//List_Serv=new LinkedList();
			System.out.println("démarage du serveur Cftp [ OK ]");
		}
		catch(IOException e)
		{
			System.out.println("démarage du serveur Cftp [ Erreur ]");
		}
	}
	protected void setport(int p)
	{ 
		port=p;
	}
	public void start()
	{
		while(true)
		{
			System.out.println("En attente d'une nouvelle connexion...\r\n");
			try
			{
				Socket s1=sock.accept();
				System.out.println("Nouvelle connexion de "+s1.getLocalAddress()+" : "+s1.getLocalPort()+".\r\n");
				System.out.println("Lancement du serveur hybride [ OK ].");
				Thread t=new Thread(new serveur_hybrid(s1,sock));
				t.start();//La
				System.out.println("Serveur hybride prêt!");
				System.out.println("\r\n\r\n\r\n");				
			}
			catch(IOException e)
			{
				System.out.println("Une erreur c'est produite l'ors d'une connexion");
				//e.printstackTrace();
				break;
			}
			
			
		}
	}
}

