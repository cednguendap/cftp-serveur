import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Enumeration;
import java.io.IOException;
import java.net.Socket;

class commande
{
	private ManagerFileSystem manager;
	private serveur_hybrid serv;
	private User user;
	
	public commande(serveur_hybrid sev,User us)
	{
		manager=new ManagerFileSystem();
		serv=sev;
		user=us;
	}
	public String[] exec(String [] cmd)
	{
		String[] tab=new String[3];
		if(cmd[0].equals("USER"))//Doit avoir une classe user
		{
			if(user.authentificate())
			{
				tab[0]="131-requette réussite";
				tab[1]=user.loggin()+" "+user.password();
				tab[2]="131";	
			}
			else
			{
				user.setLoggin(cmd[1]);
				tab[0]="331 ok, Entrez le mot de passe:";
				tab[1]=" ";
				tab[2]=" ";
			}
		}
		else if(cmd[0].equals("PASS"))
		{
			if(cmd[1].equals("anonymous") || !cmd[1].equals(" "))
			{
				tab[0]="230 Authentification reussi ";
				user.setPassword(cmd[1]);
				user.setAuthentificate(true);
			}
			else
				tab[0]="430 Login ou mot de passe invalid.";
			tab[1]=" ";
			tab[2]=" ";
		}
		else if(cmd[0].equals("PASV"))
		{
			if(user.authentificate())
			{
				tab[0]="227";
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
			}
			tab[1]=" ";
			tab[2]=" ";			
		}
		else if(cmd[0].equals("QUIT") || cmd[0].equals("CLOSE"))
		{
			tab[0]=" ";
			tab[1]=" ";
			tab[2]=" ";
			serv.setRunning(false);
		}
		else if(cmd[0].equals("MKD") || cmd[0].equals("XMKD"))
		{
			if(user.authentificate())
			{
				try
				{
					manager.mkdir(cmd[1]);
					tab[0]="257 repertoire \""+manager.CheminAbsolueVers(cmd[1])+" a été crée";
					tab[1]=" ";
				} 
				catch(IOException e)
				{
					//Message d'erreur
					tab[0]="521-"+manager.CheminAbsolueVers(cmd[1])+" existe deja";
					tab[1]="521 l'action n'a pas été prise en compte";
				} 
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
			}
			tab[2]=" ";
			
		}
		else if(cmd[0].equals("CWD"))
		{
			if(user.authentificate())
			{
				try
				{
					manager.cd(cmd[1]);
					tab[0]="200 repertoire changer vers \""+manager.CheminAbsolueVers(cmd[1])+"\"";//Suppression de fichier
				} 
				catch(IOException e)
				{
					tab[0]="431 "+e.getMessage();
				}
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
			}
			tab[1]=" ";
			tab[2]=" ";
		}
		else if(cmd[0].equals("RMD"))
		{
			if(user.authentificate())
			{
				try
				{
					manager.rmdir(cmd[1]);
					tab[0]="231 requette réussite";//Suppression de repertoire
				}
				catch(IOException e)
				{
					tab[0]="550 "+e.getMessage();
				}
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
			}
			tab[1]=" ";
			tab[2]=" ";
			
		}
		else if(cmd[0].equals("SITE"))
		{
			tab[0]="131-requette réussite";
			tab[1]=" Serveur FTP alias CFTP situé a bangangté. Par Nguendap Bedjama Cedric";
			tab[2]="131";
		}
		else if(cmd[0].equals("ACCT"))
		{
			if(user.authentificate())
			{
				tab[0]="131-requette réussite";
				tab[1]=" anonyme";
				tab[2]="131";
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}

		}
		else if(cmd[0].equals("PWD") || cmd[0].equals("XPWD"))
		{
			if(user.authentificate())
			{
				tab[0]="257 "+manager.pwd();//Chemin absolu vers le repertoire crourant
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
			}
			tab[1]=" ";
			tab[2]=" ";
		}
		else if(cmd[0].equals("SYST"))
		{
			tab[0]="215 UNIX LINUX Cedric Systeme de fichier ";//Information sur le systeme d'éxploitation
			tab[1]=" ";
			tab[2]=" ";
		}
		else if(cmd[0].equals("MLSD"))
		{
			if(user.authentificate())
			{
				tab[0]="231-requette réussite";
				tab[1]=" ";
				tab[2]="231";
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}			
		}
		else if(cmd[0].equals("NLST"))
		{

			if(user.authentificate())
			{
				if(!serv.mode())
				{
					//Mode passif on recoit un port sur lequel se connecter
					try
					{
						String res=manager.ls_l(cmd.length==1?".":cmd[1]); 
						serv.write("150 Envois en mode ASCIL");
						serv.connexionDonneEnvoi(res);
						tab[0]="226 Transfert complet";
						tab[1]=" ";
						tab[2]=" ";
					}
					catch(IOException e)
					{
						tab[0]="550 "+e.getMessage();
						tab[1]=" ";
						tab[2]=" ";
					}
				}
				else
				{
					//Mode actif on creer un port de connexion pour l'envoi

				}
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}
		}
		else if(cmd[0].equals("TYPE"))
		{
			if(user.authentificate())
			{
				serv.setTransfertMode(cmd[1].charAt(0));
				tab[0]="200 Commande de Mode Ok"+serv.transfertMode();
				tab[1]=" ";
				tab[2]=" ";
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}
		}
		else if(cmd[0].equals("PORT"))
		{
			if(user.authentificate())
			{
				tab[0]="200 Commande de PORT Ok";
				tab[1]=" ";
				tab[2]=" ";
				serv.setPortDonne(cmd[1]);
				serv.setMode(false);
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}
		}
		else if(cmd[0].equals("LIST"))
		{
			if(user.authentificate())
			{
				if(!serv.mode())
				{
					//Mode passif on recoit un port sur lequel se connecter
					try
					{
						String res=manager.ls(cmd.length==1?".":cmd[1]);
						serv.write("150 Connexion de donné ouvert en mode "+serv.transfertMode().toUpperCase());
						serv.connexionDonneEnvoi(res);
						tab[0]="226 Transfert complet ";
						tab[1]=" ";
						tab[2]=" ";
					}
					catch(IOException e)
					{
						tab[0]="550 "+e.getMessage();
						tab[1]=" ";
						tab[2]=" ";
					}
				}
				else
				{
					//Mode actif on creer un port de connexion pour l'envoi

				}
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}
		}
		else if(cmd[0].toUpperCase().equals("RETR"))
		{
			if(user.authentificate())
			{
				if(!serv.mode())
				{
					//Mode passif on recoit un port sur lequel se connecter
					try
					{
						serv.write("150 Envois en mode "+serv.transfertMode().toUpperCase()+" pour "+cmd[1]);
						Socket socTmp=serv.getSocketDonne();
						manager.get(cmd[1],serv.OutputStreamDonne(socTmp));
						serv.closeSocketDonne(socTmp);
						tab[0]=" ";
						tab[1]=" ";
						tab[2]=" ";
					}
					catch(IOException e)
					{
						tab[0]="550 "+e.getMessage();
						tab[1]=" ";
						tab[2]=" ";
					}
				}
				else
				{
					//Mode actif on creer un port de connexion pour l'envoi

				}
			}
		}
		else if(cmd[0].toUpperCase().equals("STOR"))
		{
			if(user.authentificate())
			{
				if(!serv.mode())
				{
					//Mode passif on recoit un port sur lequel se connecter
					try
					{
						serv.write("150 reception en mode "+serv.transfertMode().toUpperCase()+" pour "+cmd[1]);
						Socket socTmp=serv.getSocketDonne();
						manager.put(cmd[1],serv.InputStreamDonne(socTmp));
						serv.closeSocketDonne(socTmp);
						tab[0]="226 Transfert complet";
						tab[1]=" ";
						tab[2]=" ";
					}
					catch(IOException e)
					{
						tab[0]="550 "+e.getMessage();
						tab[1]=" ";
						tab[2]=" ";
					} 
				}
				else
				{
					//Mode actif on creer un port de connexion pour l'envoi

				}
			}
			else
			{
				tab[0]="530 Veuillez vous connecter";
				tab[1]=" ";
				tab[2]=" ";
			}
		}
		/*else if(cmd[0].equals(""))
		{

		}*/
		else
		{
			tab[0]="202 Commande non prise en compte";
			tab[1]=" ";
			tab[2]=" ";
		}
		return tab;
	}
}
